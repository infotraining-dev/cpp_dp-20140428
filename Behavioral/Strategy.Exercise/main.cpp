#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>
#include <fstream>
#include <iterator>
#include <list>
#include <stdexcept>
#include <memory>
#include <boost/bind.hpp>

struct StatResult
{
	std::string description;
	double value;

	StatResult(const std::string& desc, double val) : description(desc), value(val)
	{
	}
};

class Results
{
public:
	typedef std::list<StatResult>::const_iterator ResultIterator;

    void add_result(const StatResult& result)
	{
		results_.push_back(result);
	}

	ResultIterator begin() const
	{
		return results_.begin();
	}

	ResultIterator end() const
	{
		return results_.end();
	}

	void clear()
	{
		results_.clear();
	}

private:	
	std::list<StatResult> results_;
};

class Statistics
{
public:
    virtual void calculate(const std::vector<double>& data, Results& results) = 0;
    virtual ~Statistics() {}
};

class Avg : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        StatResult result("AVG", avg);
        results.add_result(result);
    }
};

class MinMax : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double min = *(std::min_element(data.begin(), data.end()));
        double max = *(std::max_element(data.begin(), data.end()));

        results.add_result(StatResult("MIN", min));
        results.add_result(StatResult("MAX", max));
    }
};

class Sum : public Statistics
{
public:
    void calculate(const std::vector<double>& data, Results& results)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        results.add_result(StatResult("SUM", sum));
    }
};

typedef std::shared_ptr<Statistics> StatisticsPtr;

class StatGroup : public Statistics
{
    std::vector<StatisticsPtr> stats_;
public:
    void add(StatisticsPtr stat)
    {
        stats_.push_back(stat);
    }

    void calculate(const std::vector<double>& data, Results& results)
    {
        std::for_each(stats_.begin(), stats_.end(),
                      boost::bind(&Statistics::calculate, _1,
                                  boost::cref(data),
                                  boost::ref(results)));
    }

};

class DataAnalyzer
{
    StatisticsPtr statistics_;
	std::vector<double> data_;
public:
    DataAnalyzer(StatisticsPtr statistics) : statistics_(statistics)
	{
	}

	void load_data(const std::string& file_name)
	{
		data_.clear();

		std::ifstream fin(file_name.c_str());
		if (!fin)
			throw std::runtime_error("File not opened");

		double d;
		while (fin >> d)
		{
			data_.push_back(d);
		}

		std::cout << "File " << file_name << " has been loaded...\n";
	}

	void save_data(const std::string& file_name) const
	{ 
		std::ofstream fout(file_name.c_str());
		if (!fout)
			throw std::runtime_error("File not opened");

		for(std::vector<double>::const_iterator it = data_.begin(); it != data_.end(); ++it)
			fout << (*it) << std::endl;
	}

    void set_statistics(StatisticsPtr statistics)
	{
        statistics_ = statistics;
	}

	void calculate(Results& results)
	{
        statistics_->calculate(data_, results);
	}
};

void print_results(const Results& results)
{
	for(Results::ResultIterator it = results.begin(); it != results.end(); ++it)
		std::cout << it->description << " = " << it->value << std::endl;
}

int main()
{
	Results results;

    StatisticsPtr avg = std::make_shared<Avg>();
    StatisticsPtr minmax = std::make_shared<MinMax>();
    StatisticsPtr sum = std::make_shared<Sum>();

    std::shared_ptr<StatGroup> std_stats = std::make_shared<StatGroup>();
    std_stats->add(avg);
    std_stats->add(minmax);
    std_stats->add(sum);


    DataAnalyzer da(std_stats);
	da.load_data("data.dat");

    da.calculate(results);

//    da.set_statistics(minmax);
//	da.calculate(results);

//    da.set_statistics(sum);
//	da.calculate(results);

	print_results(results);

	std::cout << "\n\n";

	results.clear();
	da.load_data("new_data.dat");
	da.calculate(results);

	print_results(results);
}
