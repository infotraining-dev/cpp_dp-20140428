#include "image.hpp"
#include "clone_factory.hpp"

namespace
{
    using namespace Drawing;

    bool is_registered
        = ShapeFactory::instance().register_shape("Image", new Image());

}
